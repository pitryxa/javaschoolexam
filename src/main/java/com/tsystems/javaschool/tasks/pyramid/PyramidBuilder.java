package com.tsystems.javaschool.tasks.pyramid;

import java.util.Comparator;
import java.util.List;

public class PyramidBuilder {

    /**
     * Builds a pyramid with sorted values (with minumum value at the top line and maximum at the bottom,
     * from left to right). All vacant positions in the array are zeros.
     *
     * @param inputNumbers to be used in the pyramid
     * @return 2d array with pyramid inside
     * @throws {@link CannotBuildPyramidException} if the pyramid cannot be build with given input
     */
    public int[][] buildPyramid(List<Integer> inputNumbers) {
        // TODO : Implement your solution here
        int size = inputNumbers.size();

        if (!canBuildPyramid(size)) {
            throw new CannotBuildPyramidException();
        }

        int rows = (int) (Math.sqrt(8 * size + 1) - 1) / 2;
        int[][] pyramid = new int[rows][2 * rows - 1];

        try {
            inputNumbers.sort(Comparator.naturalOrder());
        } catch (NullPointerException e) {
            throw new CannotBuildPyramidException();
        }

        int inputIndex = size - 1;

        for (int i = rows - 1; i >= 0; i--) {
            for (int j = rows - 1 + i; j >= rows - 1 - i; j -= 2) {
                pyramid[i][j] = inputNumbers.get(inputIndex--);
            }
        }

        return pyramid;
    }

    private boolean canBuildPyramid(int size) {
        return (Math.sqrt(8 * size + 1) - 1) / 2 % 1 == 0;
    }
}
