package com.tsystems.javaschool.tasks.subsequence;

import java.util.List;
import java.util.Objects;

public class Subsequence {

    /**
     * Checks if it is possible to get a sequence which is equal to the first
     * one by removing some elements from the second one.
     *
     * @param x first sequence
     * @param y second sequence
     * @return <code>true</code> if possible, otherwise <code>false</code>
     */
    @SuppressWarnings("rawtypes")
    public boolean find(List x, List y) {
        // TODO: Implement the logic here
        if (x == null || y == null) {
            throw new IllegalArgumentException();
        }

        int sizeX = x.size();
        int sizeY = y.size();
        int shiftY = 0;
        int find = 0;

        for (int i = 0; i < sizeX; i++) {
            if (sizeX - i > sizeY - shiftY) {
                return false;
            }

            for (int j = shiftY; j < sizeY; j++) {
                if (Objects.equals(x.get(i), y.get(j))) {
                    find++;
                    shiftY = ++j;
                    break;
                }
            }
        }

        return find == sizeX;
    }
}
