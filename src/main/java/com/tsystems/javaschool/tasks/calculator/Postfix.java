package com.tsystems.javaschool.tasks.calculator;

import java.math.BigDecimal;
import java.math.MathContext;
import java.math.RoundingMode;
import java.util.ArrayDeque;
import java.util.Deque;

public class Postfix {
    private final String expression;

    public Postfix(String expression) {
        this.expression = expression;
    }

    private Type type(String s) {
        if (s.matches("\\d+(\\.\\d+)?")) {
            return Type.NUMBER;
        } else if (s.matches("[-+*/]")) {
            return Type.OPERATOR;
        } else {
            return Type.UNKNOWN;
        }
    }

    public String calculate() {
        if (expression == null) {
            return null;
        }

        String[] elements = expression.split("\\s+");
        Deque<BigDecimal> stackNumbers = new ArrayDeque<>();

        for (String e : elements) {
            switch (type(e)) {
                case NUMBER:
                    stackNumbers.offerLast(new BigDecimal(e));
                    break;
                case OPERATOR:
                    if (stackNumbers.size() < 2) {
                        return null;
                    }
                    try {
                        stackNumbers.offerLast(execOperation(stackNumbers.pollLast(), stackNumbers.pollLast(), e));
                    } catch (ArithmeticException ex) {
                        return null;
                    }
                    break;
                case UNKNOWN:
                    return null;
            }
        }

        return stackNumbers.isEmpty()
                ? null
                : stackNumbers.pollLast().setScale(4, RoundingMode.HALF_UP).stripTrailingZeros().toPlainString();
    }

    private BigDecimal execOperation(BigDecimal b, BigDecimal a, String e) {
        BigDecimal result = BigDecimal.ONE;

        switch (new Operator(e).get()) {
            case ADDITION:
                result = b.add(a);
                break;
            case SUBTRACTION:
                result = a.subtract(b);
                break;
            case MULTIPLICATION:
                result = b.multiply(a);
                break;
            case DIVISION:
                result = a.divide(b, MathContext.DECIMAL128);
                break;
            default:
                break;
        }

        return result;
    }
}
