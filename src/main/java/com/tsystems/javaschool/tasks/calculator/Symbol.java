package com.tsystems.javaschool.tasks.calculator;

public class Symbol {
    private final char symbol;

    Symbol(char symbol) {
        this.symbol = symbol;
    }

    public Type type() {
        if (symbol == 46) {
            return Type.DOT;
        } else if (symbol >= 40 && symbol <= 43 ||
                symbol == 45 ||
                symbol == 47) {
            return Type.OPERATOR;
        } else if (symbol >= 48 && symbol <= 57) {
            return Type.NUMBER;
        } else {
            return Type.UNKNOWN;
        }
    }

    public String toString() {
        return Character.toString(symbol);
    }

    public char toChar() {
        return symbol;
    }
}
