package com.tsystems.javaschool.tasks.calculator;

import java.util.ArrayDeque;
import java.util.Deque;

public class Infix {
    private final String expression;

    public Infix(String expression) {
        this.expression = expression;
    }

    public String toPostfix() {
        StringBuilder postfix = new StringBuilder("");
        Deque<Operator> stackOperators = new ArrayDeque<>();

        char[] chars = expression.toCharArray();

        for (int i = 0; i < chars.length; i++) {
            Symbol curSym = new Symbol(chars[i]);
            Symbol prevSym = null;
            if (i > 0) {
                prevSym = new Symbol(chars[i - 1]);
            }

            switch (curSym.type()) {
                case NUMBER:
                    if (prevSym != null &&
                            prevSym.type() == Type.OPERATOR) {
                        postfix.append(' ');
                    }
                    postfix.append(curSym.toChar());
                    break;
                case DOT:
                    if (prevSym == null ||
                            prevSym.type() != Type.NUMBER) {
                        return null;
                    }
                    postfix.append(chars[i]);
                    break;
                case OPERATOR:
                    if (!operatorProcessing(postfix, stackOperators, curSym, prevSym)) {
                        return null;
                    }
                    break;
                case UNKNOWN:
                    return null;
            }
        }

        while (!stackOperators.isEmpty()) {
            if (stackOperators.peekLast().isLeftPar()) {
                return null;
            }
            postfix.append(' ').append(stackOperators.pollLast().toChar());
        }

        return postfix.toString().trim();
    }

    private boolean operatorProcessing(StringBuilder postfix, Deque<Operator> stack, Symbol curSym, Symbol prevSym) {
        Operator operator = new Operator(curSym);

        switch (operator.get()) {
            case ADDITION:
            case SUBTRACTION:
            case DIVISION:
            case MULTIPLICATION:
                if (    prevSym == null ||
                        prevSym.toString().matches("[-+(*/.]")) {
                        return false;
                }
                while ( !stack.isEmpty() &&
                        operator.compareTo(stack.peekLast()) <= 0) {
                        postfix.append(' ').append(stack.pollLast().toChar());
                }
                stack.offerLast(operator);
                break;
            case LEFT_PARENTHESIS:
                if (    prevSym != null &&
                        prevSym.toString().matches("[\\d.)]")) {
                        return false;
                }
                stack.offerLast(operator);
                break;
            case RIGHT_PARENTHESIS:
                if (    stack.isEmpty() ||
                        prevSym == null ||
                        prevSym.type() != Type.NUMBER) {
                        return false;
                }
                Operator operatorFromStack;
                while (!(operatorFromStack = stack.pollLast()).isLeftPar()) {
                    if (stack.isEmpty()) {
                        return false;
                    }
                    postfix.append(' ').append(operatorFromStack.toChar());
                }
                break;
            case UNKNOWN:
                return false;
        }
        return true;
    }
}
