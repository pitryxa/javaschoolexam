package com.tsystems.javaschool.tasks.calculator;

public enum Type {
    NUMBER,
    DOT,
    OPERATOR,
    UNKNOWN
}
